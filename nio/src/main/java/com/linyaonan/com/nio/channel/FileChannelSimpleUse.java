package com.linyaonan.com.nio.channel;

import lombok.Cleanup;
import lombok.SneakyThrows;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author: Lin
 * @date: 2020/11/7
 */
public class FileChannelSimpleUse {

    @SneakyThrows
    public static void main(String[] args) {
        @Cleanup FileInputStream fileInputStream = new FileInputStream("hello.txt");
        @Cleanup FileOutputStream fileOutputStream = new FileOutputStream("save.txt");

        @Cleanup FileChannel readChannel = fileInputStream.getChannel();
        @Cleanup FileChannel writeChannel = fileOutputStream.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate(1024);
        while (readChannel.read(buffer) != -1) {
            buffer.flip();
            writeChannel.write(buffer);
            buffer.clear();
        }
    }
}
