package com.linyaonan.com.nio.selector.udp.non_blocking_io;

import lombok.SneakyThrows;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;

/**
 * @author: Lin
 * @date: 2020/11/15
 */
public class Server {

    @SneakyThrows
    public static void main(String[] args) {
        DatagramChannel datagramChannel = DatagramChannel.open();
        // 1. 设置为非阻塞
        datagramChannel.configureBlocking(false);
        // 2. 绑定
        datagramChannel.bind(new InetSocketAddress(9998));
        // 3. 设置选择器
        Selector selector = Selector.open();
        // 4. 用于监听处理READ请求
        datagramChannel.register(selector, SelectionKey.OP_READ);
        ByteBuffer buffer = ByteBuffer.allocate(128);
        // 5. 处理监听器响应的请求
        while (selector.select() > 0) {
            // 6. 就绪的连接集合
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey selectionKey = iterator.next();
                if (selectionKey.isReadable()) {
                    while (datagramChannel.receive(buffer) != null) {
                        buffer.flip();
                        byte[] bytes = new byte[buffer.limit()];
                        buffer.get(bytes);
                        System.out.println(new String(bytes));
                        buffer.clear();
                    }
                }
                // 处理一个连接，需要进行删除
                iterator.remove();
            }
        }
    }
}
