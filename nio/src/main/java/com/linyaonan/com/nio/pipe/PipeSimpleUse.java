package com.linyaonan.com.nio.pipe;

import lombok.SneakyThrows;

import java.nio.ByteBuffer;
import java.nio.channels.Pipe;

/**
 * @author: Lin
 * @date: 2020/11/15
 */
public class PipeSimpleUse {

    @SneakyThrows
    public static void main(String[] args) {
        Pipe pipe = Pipe.open();
        // 写channel
        Pipe.SinkChannel sinkChannel = pipe.sink();

        ByteBuffer buffer = ByteBuffer.allocate(128);
        buffer.put("pipe".getBytes());
        buffer.flip();

        sinkChannel.write(buffer);
        buffer.clear();
        // 读channel
        Pipe.SourceChannel sourceChannel = pipe.source();

        sourceChannel.read(buffer);
        buffer.flip();
        byte[] bytes = new byte[buffer.limit()];
        buffer.get(bytes);
        System.out.println(new String(bytes));

    }

}
