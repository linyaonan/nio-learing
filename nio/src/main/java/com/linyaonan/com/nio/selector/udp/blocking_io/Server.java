package com.linyaonan.com.nio.selector.udp.blocking_io;

import lombok.Cleanup;
import lombok.SneakyThrows;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

/**
 * @author: Lin
 * @date: 2020/11/15
 */
public class Server {

    @SneakyThrows
    public static void main(String[] args) {
        @Cleanup DatagramChannel datagramChannel = DatagramChannel.open();
        datagramChannel.bind(new InetSocketAddress(8889));
        ByteBuffer buffer = ByteBuffer.allocate(128);
        while (datagramChannel.receive(buffer) != null) {
            buffer.flip();
            byte[] bytes = new byte[buffer.limit()];
            buffer.get(bytes);
            System.out.println(new String(bytes));
            buffer.clear();
        }
    }
}
