package com.linyaonan.com.nio.selector.tcp.non_blocking_io;

import lombok.Cleanup;
import lombok.SneakyThrows;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @author: Lin
 * @date: 2020/11/15
 */
public class Client {

    @SneakyThrows
    public static void main(String[] args) {
        // 1. 通道
        @Cleanup SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress("127.0.0.1", 9999));

        // 2. 切换为非阻塞
        socketChannel.configureBlocking(false);

        // 3. 缓冲区
        ByteBuffer buffer = ByteBuffer.allocate(128);
        // 4. 写入缓冲区数据
        byte[] bytes = "hello non—blocking io".getBytes();
        buffer.put(bytes);
        buffer.flip();
        socketChannel.write(buffer);
    }

}
