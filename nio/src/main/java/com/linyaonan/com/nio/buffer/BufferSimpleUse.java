package com.linyaonan.com.nio.buffer;


import java.nio.ByteBuffer;

/**
 * @author: Lin
 * @date: 2020/11/7
 */
public class BufferSimpleUse {

    public static void main(String[] args) {
        // 1. 创建一个buffer，默认长度10
        System.out.println("=====================1. 创建阶段=============================");
        ByteBuffer buffer = ByteBuffer.allocate(10);
        showBufferInfo(buffer);

        // 2. 写入内容【默认为写模式】
        System.out.println("=====================2. 写入阶段=============================");
        byte[] bytes = "abc".getBytes();
        buffer.put(bytes);
        showBufferInfo(buffer);

        // 3. 切换为读模式
        System.out.println("=====================3. 读取阶段=============================");
        buffer.flip();
        byte[] save = new byte[buffer.limit()];
        buffer.get(save);
        System.out.println("get = " + new String(save));
        showBufferInfo(buffer);

        // 4. 清空
        System.out.println("=====================4. clear阶段=============================");
        buffer.clear();
        showBufferInfo(buffer);

        // 5. 尝试读取
        System.out.println("=====================5. clear后继续读取=============================");
        char c = (char)buffer.get(0);
        System.out.println("try read = " + (c + ""));
        showBufferInfo(buffer);

        // 6. 重新写
        System.out.println("=====================6. 写入阶段2=============================");
        byte[] bytes2 = "abcd".getBytes();
        buffer.put(bytes2);
        showBufferInfo(buffer);

        // 7. 重新切换为读模式
        System.out.println("=====================7. 读取阶段2=============================");
        buffer.flip();
        byte[] save2 = new byte[buffer.limit()];
        buffer.get(save2);
        System.out.println("get2 = " + new String(save2));
        showBufferInfo(buffer);

        // 8. rewind 重新读
        System.out.println("=====================8. 重读阶段=============================");
        buffer.rewind();
        byte[] save3 = new byte[buffer.limit()];
        buffer.get(save3);
        System.out.println("get3 = " + new String(save3));
        showBufferInfo(buffer);

        // 9. 标记阶段
        System.out.println("=====================9. 标记阶段=============================");
        // 首先将指针指向归位
        buffer.rewind();
        byte[] read = new byte[1];
        buffer.get(read);
        System.out.println("读取出指向位置的字符 = " + new String(read));
        showBufferInfo(buffer);
        // 对当前position进行标记
        buffer.mark();
        showBufferInfo(buffer);

        // 10. reset阶段
        System.out.println("=====================10. reset阶段=============================");
        showBufferInfo(buffer);
        // 直接手动跳转到3的位置
        buffer.position(3);
        buffer.get(read);
        // 取出的内容为d
        System.out.println("读取出指向位置的字符 = " + new String(read));
        // reset后，position复位为原来mark的位置
        System.out.println("=====================reset复位=============================");
        buffer.reset();
        showBufferInfo(buffer);
        buffer.get(read);
        // 取出的内容为d
        System.out.println("读取出指向位置的字符 = " + new String(read));
        showBufferInfo(buffer);

        // 11. remaining
        System.out.println("=====================11. remaining阶段=============================");
        showBufferInfo(buffer);
        int remaining = buffer.remaining();
        boolean hasRemaining = buffer.hasRemaining();
        System.out.println("是否存在未取出的数据：" + hasRemaining + ", position 与 limit 之间还有：" + remaining);
    }

    private static void showBufferInfo(ByteBuffer buffer) {
        // 1. 容量
        System.out.println("【capacity】：容量 = " + buffer.capacity());
        // 2. 操作指针
        System.out.println("【position】：操作指针 = " + buffer.position());
        // 3. 操作边界
        System.out.println("【limit】：操作边界 = " + buffer.limit());
        // 4. 上一次的标记位
        System.out.println("【mark】：上一次的标记位 = " + buffer.mark());

    }

}
