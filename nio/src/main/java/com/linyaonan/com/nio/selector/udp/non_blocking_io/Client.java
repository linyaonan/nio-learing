package com.linyaonan.com.nio.selector.udp.non_blocking_io;

import lombok.Cleanup;
import lombok.SneakyThrows;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

/**
 * @author: Lin
 * @date: 2020/11/15
 */
public class Client {

    @SneakyThrows
    public static void main(String[] args) {
        @Cleanup DatagramChannel datagramChannel = DatagramChannel.open();
        // 设置为非阻塞
        datagramChannel.configureBlocking(false);
        ByteBuffer buffer = ByteBuffer.allocate(128);
        byte[] bytes = "udp non-blocking io".getBytes();
        buffer.put(bytes);
        buffer.flip();
        datagramChannel.send(buffer, new InetSocketAddress("127.0.0.1", 9998));
    }

}
